# jWorkday

jWorkday is a small library to calculate national workdays / holidays for various locales.
Currently only non-workdays in Norway are supported.

**Maven:**

```
<dependency>
    <groupId>no.kodeworks</groupId>
    <artifactId>jworkday</artifactId>
    <version>0.1</version>
</dependency>
```

**Usage:**

```java
List<LocalDate> nonWorkdays = NonWorkdays.apply(2018);
System.out.println(nonWorkdays);
```
Prints:
```
[2018-01-01, 2018-03-29, 2018-03-30, 2018-04-02, 2018-05-01, 2018-05-10, 2018-05-17, 2018-05-21, 2018-12-25, 2018-12-26]
```
These are all non-workdays that fall on weekdays. Non-workdays on Saturdays and Sundays are excluded.

**License**

Apache License, Version 2.0
http://www.apache.org/licenses/LICENSE-2.0.txt