package no.kodeworks.jworkday;

import org.shredzone.commons.suncalc.MoonIllumination;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Calculates all weekdays for a given year that are non-work days.
 * Non-work days that fall on saturdays or sundays are not included in the apply method,
 * but can be queried explicitly with each of the given methods.
 */
public final class NonWorkdays {
    private static final TimeZone TIME_ZONE_NO = TimeZone.getTimeZone("NO");
    private static double SECS_PER_MOON_MONTH = 29.53 * 24d * 60d * 60d;
    private static double DEG_PER_SEC = 360d / SECS_PER_MOON_MONTH;

    public static List<LocalDate> apply(int year) {
        LocalDate foerstePaaskedag = foerstePaaskedag(year);
        ArrayList<LocalDate> nonWorkDays = new ArrayList<>(10);
        addIfWeekday(foersteNyttaarsdag(year), nonWorkDays);
        nonWorkDays.add(skjaertorsdag(foerstePaaskedag));
        nonWorkDays.add(langfredag(foerstePaaskedag));
        nonWorkDays.add(andrePaaskedag(foerstePaaskedag));
        addIfWeekday(foersteMai(year), nonWorkDays);
        nonWorkDays.add(kristiHimmelfartsdag(foerstePaaskedag));
        addIfWeekday(grunnlovsdag(year), nonWorkDays);
        nonWorkDays.add(andrePinsedag(foerstePaaskedag));
        addIfWeekday(foersteJuledag(year), nonWorkDays);
        addIfWeekday(andreJuledag(year), nonWorkDays);
        return nonWorkDays;
    }

    private static void addIfWeekday(LocalDate date, List<LocalDate> dates) {
        if (date.getDayOfWeek().getValue() < 6)
            dates.add(date);
    }

    // Movable Days - Start

    public static LocalDate foerstePaaskedag(int year) {
        return firstSundayAfter(firstFullMoonAfter(LocalDate.of(year, 3, 21).atStartOfDay()));
    }

    public static LocalDate firstFullMoonAfter(LocalDateTime date) {
        MoonIllumination.Parameters temp = MoonIllumination.compute()
                .timezone(TIME_ZONE_NO)
                .on(Date.from(date.toInstant(ZoneOffset.UTC)));
        double start = temp.execute().getPhase();
        double secs = secs(start, DEG_PER_SEC);
        temp = temp.on(Date.from(date.plusSeconds((long) secs).toInstant(ZoneOffset.UTC)));
        double end = temp.execute().getPhase();
        double dist = end - start;
        if (dist < 0d) dist += 360d;
        else if (360d < dist) dist -= 360d;
        secs = secs(start, dist / secs);
        return date.plusSeconds((long) secs).toLocalDate();
    }

    private static double secs(double start, double degPerSec) {
        double secs = -1d * start / degPerSec;
        if (secs < 0d) secs = secs + SECS_PER_MOON_MONTH;
        return secs;
    }

    public static LocalDate firstSundayAfter(LocalDate date) {
        return date.with(TemporalAdjusters.next(DayOfWeek.SUNDAY));
    }

    public static LocalDate palmesoendag(int year) {
        return palmesoendag(foerstePaaskedag(year));
    }

    public static LocalDate palmesoendag(LocalDate foerstePaaskedag) {
        return foerstePaaskedag.with(TemporalAdjusters.previous(DayOfWeek.SUNDAY));
    }

    public static LocalDate skjaertorsdag(int year) {
        return skjaertorsdag(foerstePaaskedag(year));
    }

    public static LocalDate skjaertorsdag(LocalDate foerstePaaskedag) {
        return foerstePaaskedag.with(TemporalAdjusters.previous(DayOfWeek.THURSDAY));
    }

    public static LocalDate langfredag(int year) {
        return langfredag(foerstePaaskedag(year));
    }

    public static LocalDate langfredag(LocalDate foerstePaaskedag) {
        return foerstePaaskedag.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY));
    }

    public static LocalDate andrePaaskedag(int year) {
        return langfredag(andrePaaskedag(year));
    }

    public static LocalDate andrePaaskedag(LocalDate foerstePaaskedag) {
        return foerstePaaskedag.plusDays(1L);
    }

    public static LocalDate kristiHimmelfartsdag(int year) {
        return langfredag(kristiHimmelfartsdag(year));
    }

    public static LocalDate kristiHimmelfartsdag(LocalDate foerstePaaskedag) {
        return foerstePaaskedag.plusDays(39L);
    }

    public static LocalDate foerstePinsedag(int year) {
        return langfredag(foerstePinsedag(year));
    }

    public static LocalDate foerstePinsedag(LocalDate foerstePaaskedag) {
        return foerstePaaskedag.plusDays(49L);
    }

    public static LocalDate andrePinsedag(int year) {
        return langfredag(foerstePinsedag(year));
    }

    public static LocalDate andrePinsedag(LocalDate foerstePaaskedag) {
        return foerstePaaskedag.plusDays(50L);
    }

    // Movable Days - End

    // Fixed Days - Start

    public static LocalDate foersteNyttaarsdag(int year) {
        return LocalDate.of(year, 1, 1);
    }

    public static LocalDate foersteMai(int year) {
        return LocalDate.of(year, 5, 1);
    }

    public static LocalDate grunnlovsdag(int year) {
        return LocalDate.of(year, 5, 17);
    }

    public static LocalDate foersteJuledag(int year) {
        return LocalDate.of(year, 12, 25);
    }

    public static LocalDate andreJuledag(int year) {
        return LocalDate.of(year, 12, 26);
    }

    // Fixed Days - End
}
