package no.kodeworks.jworkday;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.util.List;

public class TestNonWorkdays {
    @Test
    public void testFoerstePaaskedag() {
        LocalDate[] expecteds = new LocalDate[]{
                LocalDate.of(2007, 4, 8),
                LocalDate.of(2008, 3, 23),
                LocalDate.of(2009, 4, 12),
                LocalDate.of(2010, 4, 4),
                LocalDate.of(2011, 4, 24),
                LocalDate.of(2012, 4, 8),
                LocalDate.of(2013, 3, 31),
                LocalDate.of(2014, 4, 20),
                LocalDate.of(2015, 4, 5),
                LocalDate.of(2016, 3, 27),
                LocalDate.of(2017, 4, 16),
                LocalDate.of(2018, 4, 1),
                LocalDate.of(2019, 4, 21)
        };
        int start = 2007;
        int end = 2020;
        LocalDate[] foerstePaaskedags = new LocalDate[end - start];
        for (int year = start; year < end; year++)
            foerstePaaskedags[year - start] = NonWorkdays.foerstePaaskedag(year);
        Assert.assertArrayEquals(expecteds, foerstePaaskedags);
    }

    @Test
    public void testSkjaertorsdag() {
        LocalDate[] expecteds = new LocalDate[]{
                LocalDate.of(2018, 3, 29),
                LocalDate.of(2019, 4, 18),
                LocalDate.of(2020, 4, 9),
                LocalDate.of(2021, 4, 1),
                LocalDate.of(2022, 4, 14)
        };
        int start = 2018;
        int end = 2023;
        LocalDate[] skjaertorsdags = new LocalDate[end - start];
        for (int year = start; year < end; year++)
            skjaertorsdags[year - start] = NonWorkdays.skjaertorsdag(year);
        Assert.assertArrayEquals(expecteds, skjaertorsdags);
    }

    @Test
    public void testApply() {
        LocalDate[] nonWorkdays = NonWorkdays.apply(2018).toArray(new LocalDate[10]);
        LocalDate[] expecteds = new LocalDate[]{
                LocalDate.of(2018, 1, 1),
                LocalDate.of(2018, 3, 29),
                LocalDate.of(2018, 3, 30),
                LocalDate.of(2018, 4, 2),
                LocalDate.of(2018, 5, 1),
                LocalDate.of(2018, 5, 10),
                LocalDate.of(2018, 5, 17),
                LocalDate.of(2018, 5, 21),
                LocalDate.of(2018, 12, 25),
                LocalDate.of(2018, 12, 26)
        };
        Assert.assertArrayEquals(expecteds, nonWorkdays);

    }
}
